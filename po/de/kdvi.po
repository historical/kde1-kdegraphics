msgid ""
msgstr ""
"Project-Id-Version: kdvi 0.4.1\n"
"POT-Creation-Date: 1998-12-28 15:35+0100\n"
"PO-Revision-Date: 1999-04-23 10:00+0100\n"
"Last-Translator:Matthias Kiefer <matthias.kiefer@gmx.de>\n"
"Translator: Oliver Hensel <oliver.hensel@gmx.de>\n"

#: dviwin.cpp:215 dviwin.cpp:231 dviwin.cpp:245 dviwin.cpp:282 dviwin.cpp:299
#: kdvi.cpp:514
msgid "Notice"
msgstr "Hinweis"

#: dviwin.cpp:216
msgid ""
"The change in font generation will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Die �nderung der Schriften-Generierung tritt\n"
"erst nach einem Neustart von kdvi in Kraft!"

#: dviwin.cpp:232
msgid ""
"The change in font path will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Die �nderung des Schriftenverzeichnisses tritt\n"
"erst nach einem Neustart von kdvi in Kraft!"

#: dviwin.cpp:246
msgid ""
"The change in Metafont mode will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Die �nderung des Metafont-Modus tritt erst\n"
"nach einem Neustart von kdvi in Kraft!"

#: dviwin.cpp:283
msgid ""
"The change in resolution will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Die �nderung der Aufl�sung tritt erst\n"
"nach einem Neustart von kdvi in Kraft!"

#: dviwin.cpp:300
msgid ""
"The change in gamma will be effective\n"
"only after you start kdvi again!"
msgstr ""
"Die �nderung des Gamma-Faktors tritt erst\n"
"nach einem Neustart von kdvi in Kraft!"

#: dviwin.cpp:423 dviwin.cpp:457
msgid "HEY"
msgstr "HEY"

#: dviwin.cpp:424 dviwin.cpp:458
msgid "What's this? DVI problem!\n"
msgstr "Was ist los? DVI-Problem!\n"

#: dviwin.cpp:508
msgid "File status changed."
msgstr "Status der Datei ge�ndert."

#: dviwin.cpp:513 kdvi.cpp:937
msgid "File reloaded."
msgstr "Datei neu geladen."

#: kdvi.cpp:93
msgid "No document"
msgstr "Kein Dokument"

#: kdvi.cpp:109
msgid "Toggle Menubar"
msgstr "Men�leiste an/aus"

#: kdvi.cpp:111
msgid "Mark page"
msgstr "Seite markieren"

#: kdvi.cpp:113
msgid "Redraw"
msgstr "Neuzeichnen"

#: kdvi.cpp:115
msgid "Preferences ..."
msgstr "Einstellungen..."

#: kdvi.cpp:173
msgid "&New"
msgstr "&Neu"

#: kdvi.cpp:174
msgid "&Open ..."
msgstr "�&ffnen..."

#: kdvi.cpp:181
msgid "Open &recent"
msgstr "&Zuletzt ge�ffnete"

#: kdvi.cpp:183
msgid "&Print ..."
msgstr "&Drucken..."

#: kdvi.cpp:191
msgid "Zoom &in"
msgstr "Ver&gr��ern"

#: kdvi.cpp:192
msgid "Zoom &out"
msgstr "Ver&kleinern"

#: kdvi.cpp:193
msgid "&Fit to page"
msgstr "Passend auf &Seite"

#: kdvi.cpp:194
msgid "Fit to page &width"
msgstr "Passend auf &Breite"

#: kdvi.cpp:196
msgid "&Redraw page"
msgstr "&Neu zeichnen"

#: kdvi.cpp:203
msgid "&Previous"
msgstr "&Vorherige"

#: kdvi.cpp:204
msgid "&Next"
msgstr "&N�chste"

#: kdvi.cpp:205
msgid "&First"
msgstr "&Erste"

#: kdvi.cpp:206
msgid "&Last"
msgstr "&Letzte"

#: kdvi.cpp:207
msgid "&Go to ..."
msgstr "&Gehe zu..."

#: kdvi.cpp:210
msgid "&Page"
msgstr "&Seite"

#: kdvi.cpp:215
msgid "&Preferences ..."
msgstr "&Einstellungen..."

#: kdvi.cpp:216
msgid "&Keys ..."
msgstr "&Tasten..."

#: kdvi.cpp:218
msgid "Make PK-&fonts"
msgstr "&PK-Schriften erzeugen"

#: kdvi.cpp:220
msgid "Show PS"
msgstr "PS anzeigen"

#: kdvi.cpp:222
msgid "Show &Menubar"
msgstr "&Men�leiste anzeigen"

#: kdvi.cpp:224
msgid "Show &Buttons"
msgstr "&Werkzeugleiste anzeigen"

#: kdvi.cpp:226
msgid "Show Page Lis&t"
msgstr "&Seitenliste anzeigen"

#: kdvi.cpp:228
msgid "Show &Statusbar"
msgstr "Status&leiste anzeigen"

#: kdvi.cpp:230
msgid "Show Scro&llbars"
msgstr "S&crolleisten anzeigen"

#: kdvi.cpp:239
msgid "DVI Viewer"
msgstr "DVI Betrachter"

#: kdvi.cpp:241
msgid ""
"\n"
"\n"
"by Markku Hihnala"
msgstr ""
"\n"
"\n"
"von Markku Hihnala <mah@ee.oulu.fi>"

#: kdvi.cpp:261
msgid "Open document ..."
msgstr "Dokument �ffnen"

#: kdvi.cpp:262
msgid "Reload document"
msgstr "Dokument neu laden"

#: kdvi.cpp:263
msgid "Print ..."
msgstr "Dokument drucken"

#: kdvi.cpp:265
msgid "Go to first page"
msgstr "Erste Seite"

#: kdvi.cpp:266
msgid "Go to previous page"
msgstr "Vorherige Seite"

#: kdvi.cpp:267
msgid "Go down then top of next page"
msgstr "Nach unten, dann n�chste Seite oben"

#: kdvi.cpp:268
msgid "Go to next page"
msgstr "N�chste Seite"

#: kdvi.cpp:269
msgid "Go to last page"
msgstr "Letzte Seite"

#: kdvi.cpp:271
msgid "Decrease magnification"
msgstr "Weniger Vergr��erung"

#: kdvi.cpp:272
msgid "Small text"
msgstr "Kleiner Text"

#: kdvi.cpp:273
msgid "Large text"
msgstr "Gro�er Text"

#: kdvi.cpp:274
msgid "Increase magnification"
msgstr "Mehr Verg��erung"

#: kdvi.cpp:299
msgid "Scroll window and switch the page"
msgstr "Scrollen und Seitenwechsel"

#: kdvi.cpp:308
msgid "Select page and mark pages for printing"
msgstr "Seite w�hlen und zum Drucken markieren"

#: kdvi.cpp:323
msgid "X:0000, Y:0000 "
msgstr "X:0000, Y:0000 "

#: kdvi.cpp:325
msgid "Shrink: xx"
msgstr "Faktor: xx"

#: kdvi.cpp:327
msgid "Page: xxxx / xxxx"
msgstr "Seite: xxxx / xxxx"

#: kdvi.cpp:342 kdvi.cpp:395
msgid "New window"
msgstr "Neues Fenster"

#: kdvi.cpp:343 kdvi.cpp:396
msgid "Open file"
msgstr "Datei �ffnen"

#: kdvi.cpp:344 kdvi.cpp:397
msgid "Print dialog"
msgstr "Druckauswahl"

#: kdvi.cpp:346 kdvi.cpp:399
msgid "Zoom in"
msgstr "Vergr��ern"

#: kdvi.cpp:347 kdvi.cpp:400
msgid "Zoom out"
msgstr "Verkleinern"

#: kdvi.cpp:348 kdvi.cpp:401
msgid "Fit window"
msgstr "Passend zum Fenster"

#: kdvi.cpp:349 kdvi.cpp:402
msgid "Fit width"
msgstr "Passend zur Breite"

#: kdvi.cpp:350 kdvi.cpp:403
msgid "Redraw page"
msgstr "Neuzeichnen"

#: kdvi.cpp:351 kdvi.cpp:405
msgid "Next page"
msgstr "N�chste Seite"

#: kdvi.cpp:352 kdvi.cpp:404
msgid "Previous page"
msgstr "Vorherige Seite"

#: kdvi.cpp:353 kdvi.cpp:407
msgid "Last page"
msgstr "Letzte Seite"

#: kdvi.cpp:354 kdvi.cpp:406
msgid "First page"
msgstr "Erste Seite"

#: kdvi.cpp:355 kdvi.cpp:408
msgid "Goto page"
msgstr "Gehe zu Seite"

#: kdvi.cpp:356
msgid "Configure keys"
msgstr "Konfiguriere Tasten"

#: kdvi.cpp:357 kdvi.cpp:409
msgid "Toggle show PS"
msgstr "PS zeigen an/aus"

#: kdvi.cpp:358 kdvi.cpp:410
msgid "Toggle menu bar"
msgstr "Men�leiste an/aus"

#: kdvi.cpp:359 kdvi.cpp:411
msgid "Toggle button bar"
msgstr "Werkzeugleiste an/aus"

#: kdvi.cpp:360 kdvi.cpp:412
msgid "Toggle page list"
msgstr "Seitenliste an/aus"

#: kdvi.cpp:361 kdvi.cpp:413
msgid "Toggle status bar"
msgstr "Statusleiste an/aus"

#: kdvi.cpp:362 kdvi.cpp:414
msgid "Toggle scroll bars"
msgstr "Scrolleisten an/aus"

#: kdvi.cpp:466
msgid "File open dialog is open"
msgstr "Datei-�ffnen-Dialog offen"

#: kdvi.cpp:515
msgid "Can't read file:\n"
msgstr "Kann Datei nicht lesen:\n"

#: kdvi.cpp:521
msgid "Opening "
msgstr "�ffne "

#: kdvi.cpp:546
msgid "Opened "
msgstr "Ge�ffnet "

#: kdvi.cpp:556
msgid "Print dialog is open"
msgstr "Druckdialog offen"

#: kdvi.cpp:571
msgid "View size fits page"
msgstr "Ansichtsgr��e pa�t auf Seite"

#: kdvi.cpp:580
msgid "View width fits page"
msgstr "Ansichtsbreite pa�t auf Seite"

#: kdvi.cpp:672
msgid "Preferences applied"
msgstr "Einstellungen angewandt"

#: kdvi.cpp:676
msgid " Page "
msgstr " Seite "

#: kdvi.cpp:678
msgid "Go to page"
msgstr "Gehe zu Seite"

#: kdvi.cpp:692
msgid "Go to"
msgstr "Gehe zu"

#: kdvi.cpp:713
msgid "Missing PK-fonts will be generated"
msgstr "Fehlende PK-Schriften werden generiert"

#: kdvi.cpp:714
msgid "Missing PK-fonts will be logged to 'missfont.log'"
msgstr "Fehlende PK-Schriften werden in 'missfont.log' aufgezeichnet"

#: kdvi.cpp:730
msgid "Postcsript specials are rendered"
msgstr "Postscript-Spezialit�ten werden gezeichnet"

#: kdvi.cpp:731
msgid "Postscript specials are not rendered"
msgstr "Postscript-Spezialit�ten werden nicht gezeichnet"

#: kdvi.cpp:747
msgid "No menu bar"
msgstr "Men�leiste aus"

#: kdvi.cpp:747
msgid "Menu bar is shown"
msgstr "Men�leiste an"

#: kdvi.cpp:765
msgid "No button bar"
msgstr "Werkzeugleiste aus"

#: kdvi.cpp:765
msgid "Button bar is shown"
msgstr "Werkzeugleiste an"

#: kdvi.cpp:779
msgid "Tool bar is shown"
msgstr "Werkzeugleiste an"

#: kdvi.cpp:779
msgid "No tool bar"
msgstr "Werkzeugleiste aus"

#: kdvi.cpp:803
msgid "No status bar"
msgstr "Statusleiste aus"

#: kdvi.cpp:803
msgid "Status bar is shown"
msgstr "Statusleiste an"

#: kdvi.cpp:817
msgid "Scroll bars are shown"
msgstr "Scrolleisten an"

#: kdvi.cpp:817
msgid "No scroll bars"
msgstr "Scrolleisten aus"

#: kdvi.cpp:874
msgid "Page: "
msgstr "Seite: "

#: kdvi.cpp:874
msgid " / "
msgstr " / "

#: kdvi.cpp:905
msgid "Large text button set to shrink factor "
msgstr "Gro�er Textbutton auf Vergr��erungsfaktor gestellt: "

#: kdvi.cpp:913
msgid "Small text button set to shrink factor "
msgstr "Kleiner Textbutton auf Vergr��erungsfaktor gestellt: "

#: kdvi.cpp:931
#, c-format
msgid "Shrink: %d"
msgstr "Faktor: %d"

#: marklist.cpp:37
msgid "Mark current"
msgstr "Aktuelle markieren"

#: marklist.cpp:38
msgid "Unmark current"
msgstr "Aktuelle demarkieren"

#: marklist.cpp:39
msgid "Mark all"
msgstr "Alle markieren"

#: marklist.cpp:40
msgid "Mark even"
msgstr "Gerade markieren"

#: marklist.cpp:41
msgid "Mark odd"
msgstr "Ungerade Markieren"

#: marklist.cpp:42
msgid "Toggle marks"
msgstr "Markierung an/aus"

#: marklist.cpp:43
msgid "Remove marks"
msgstr "Entferne Markierungen"

#: prefs.cpp:45
msgid "Preferences"
msgstr "Einstellungen"

#: prefs.cpp:213
msgid "Menu Bar"
msgstr "Men�leiste"

#: prefs.cpp:214
msgid "Button Bar"
msgstr "Werkzeugleiste"

#: prefs.cpp:215
msgid "Status Bar"
msgstr "Statusleiste"

#: prefs.cpp:216
msgid "Page List"
msgstr "Seitenliste"

#: prefs.cpp:217
msgid "Scroll Bars"
msgstr "Scrolleisten"

#: prefs.cpp:239
msgid "Number of recent files"
msgstr "Anzahl zu merkender Dateien"

#: prefs.cpp:260
msgid "Generate missing fonts"
msgstr "Generiere fehlende Schriften"

#: prefs.cpp:277
msgid "Resolution"
msgstr "Aufl�sung"

#: prefs.cpp:282
msgid "Metafont mode"
msgstr "Metafont-Modus"

#: prefs.cpp:291
msgid "PK Font Path"
msgstr "PK-Schriftenverzeichnis"

#: prefs.cpp:300
msgid "Shrink factor for small text"
msgstr "Vergr��erungsfaktor f�r kleinen Text"

#: prefs.cpp:305
msgid "Shrink factor for large text"
msgstr "Vergr��erungsfaktor f�r gro�en Text"

#: prefs.cpp:325
msgid "Show postscript specials"
msgstr "Zeige Postscript-Spezialit�ten"

#: prefs.cpp:326
msgid "Antialiased postcript"
msgstr "Postscript Antialias"

#: prefs.cpp:344
msgid "Gamma"
msgstr "Gamma"

#: prefs.cpp:357
msgid "Rendering"
msgstr "Zeichne"

#: prefs.cpp:387
msgid "Paper"
msgstr "Papier"

#: print.cpp:61
msgid "Print "
msgstr "Drucken "

#: print.cpp:120
msgid "Cannot open dvi file!"
msgstr "Kann dvi-Datei nicht �ffnen!"

#: print.cpp:124
msgid "Cannot open output dvi file!"
msgstr "Kann dvi-Ausgabedatei nicht �ffnen!"

#: print.cpp:136
msgid "Cannot handle this dvi version!"
msgstr "Kann diese dvi-Version nicht bearbeiten!"

#: print.cpp:241
msgid "Invalid page range!"
msgstr "Ung�ltiger Seitenbereich"

#: print.cpp:318 printSetup.cpp:73
msgid "Default Printer"
msgstr "Standarddrucker"

#: printData.cpp:31
msgid "&File Name:"
msgstr "&Dateiname:"

#: printData.cpp:58
msgid "&All"
msgstr "&Alle"

#: printData.cpp:65
msgid "&Current"
msgstr "A&ktuelle"

#: printData.cpp:71
msgid "&Marked"
msgstr "&Markierte"

#: printData.cpp:78
msgid "&Range"
msgstr "&Bereich"

#: printData.cpp:98
msgid "Re&verse"
msgstr "&Umgekehrt"

#: printData.cpp:107
msgid "1 page on sheet"
msgstr "1 Seite pro Blatt"

#: printData.cpp:108
msgid "2 pages on sheet"
msgstr "2 Seiten pro Blatt"

#: printData.cpp:109
msgid "4 pages on sheet"
msgstr "4 Seiten pro Blatt"

#: printData.cpp:115
msgid "Fill rows"
msgstr "H�he anpassen"

#: printData.cpp:116
msgid "Fill columns"
msgstr "Breite anpassen"

#: printData.cpp:128
msgid "&Setup"
msgstr "&Einstellungen"

#: printData.cpp:143
msgid "Page order"
msgstr "Reihenfolge"

#: printData.cpp:151
msgid "Pages"
msgstr "Seiten"

#: printData.cpp:163
msgid "Print to"
msgstr "Drucken auf"

#: printSetup.cpp:23
msgid "Print Setup"
msgstr "Druckereinstellungen"

#: printSetupData.cpp:42
msgid "Printers"
msgstr "Drucker"

#: printSetupData.cpp:47
msgid "&Internal"
msgstr "&Intern"

#: printSetupData.cpp:65
msgid "Use ps&nup"
msgstr "Benutze ps&nup"

#: printSetupData.cpp:71
msgid "Use &mpage"
msgstr "Benutze &mpage"

#: printSetupData.cpp:91
msgid "&Add"
msgstr "&Hinzuf�gen"

#: printSetupData.cpp:119
msgid "&Spooler command:"
msgstr "&Druckbefehl:"

#: printSetupData.cpp:127
msgid "n-up"
msgstr "n-up"

#: printSetupData.cpp:136
msgid "Printing method"
msgstr "Druckmethode"
